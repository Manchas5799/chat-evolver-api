import requests


class EvolverApi:
    def __init__(self):
        self.url = "http://desa1.x-tendperu.com/"
        self.directions = {
            "start": "api/cli/start",
            "exec": "api/cli/exec",
        }
        self.headers = {
            'Content-Type': 'application/json',
            'token': self.get_token()
        }
        self.payload = {}

    def start(self):
        self.payload.update({})
        return self.make_request(self.directions['start'])

    def exec(self, commands: list[str], session_id):
        commands_to_send = ["set-env clitest1"]
        for command in commands:
            commands_to_send.append(command)
        self.payload.update({'Commands': commands_to_send, 'SessionId': session_id})
        return self.make_request(self.directions['exec'])

    def get_token(self):
        return 'manchas'

    def make_request(self, path):
        print("Headers ---- ")
        print(self.headers)
        print("Body -----")
        print(self.payload)
        return requests.post(self.url + path, json=self.payload, headers=self.headers)