FROM python:3.10.12-slim-buster

# Establecer el directorio de trabajo en el contenedor
WORKDIR /app

# Copiar los archivos de requerimientos
COPY requirements.txt .

# Instalar las dependencias del proyecto
RUN pip install -r requirements.txt

# Copiar el código fuente al contenedor
COPY . .

# Exponer el puerto en el que se ejecuta el proyecto
EXPOSE 8000


ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0"]