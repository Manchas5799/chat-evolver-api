from os.path import dirname, join
from typing import Annotated

import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI, Header, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel
from starlette.staticfiles import StaticFiles

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


class ChatRequest(BaseModel):
    text: str


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)
app.mount("/static", StaticFiles(directory="static"), name="static")
views = Jinja2Templates(directory="views")


@app.get("/")
def status_check():
    return {
        "status": "ok"
    }


@app.get("/chat-evolver-template")
def chat_evolver_template(request: Request):
    from templates.TemplatesService import TemplatesService
    templates = TemplatesService().get_templates()
    return views.TemplateResponse("create-template-chat.html", {"request": request, "data": {
        "templates": templates
    }})


@app.get("/api/chat-evolver-template")
def api_chat_evolver_template():
    from templates.TemplatesService import TemplatesService
    templates = TemplatesService().get_templates()
    return {
        "data": templates
    }


@app.post("/api/chat-evolver")
async def api(request: ChatRequest, sesion_id: Annotated[str | None, Header()] = None):
    from llamachain import OpenAIChat
    from EvolverApi import EvolverApi

    evolver = EvolverApi()
    if sesion_id is None:
        answer_start = evolver.start()
        sesion_id = answer_start.json()["ReturnValue"]["SessionId"]
        print("SessionId: " + sesion_id)

    chat = OpenAIChat()

    answer_model = chat.predict(request.text)
    print(answer_model)
    answers = answer_model.split("<code>")
    answers_clears = []
    for answer in answers:
        if answer.find("</code>") == -1:
            continue
        answer_cleared = answer.split("</code>")[0]
        if answer_cleared != "":
            answers_clears.append(answer_cleared)
    response = evolver.exec(answers_clears, sesion_id).json()

    return {"answer": answer_model, "response": response, "answerCleared": answers_clears, "sessionId": sesion_id}


if __name__ == "__main__":
    uvicorn.run('main:app', host="0.0.0.0", port=8000, reload=True)