import ftplib
import os


class TemplatesService:
    def __init__(self):
        self.templates = {}
        self.ftp = self.access_ftp()
        self.set_templates()

    def set_templates(self):
        self.templates = self.ftp.nlst()

        if len(self.templates) == 0:
            self.templates = self.get_templates_from_source()

    def access_ftp(self):
        try:
            ftp = ftplib.FTP(os.environ.get("FTP_HOST"))
            ftp.login(os.environ.get("FTP_USER"), os.environ.get("FTP_PASS"))
            return ftp
        except Exception as e:
            print(e)
            return None

    def get_templates_from_source(self):
        current_path = os.path.dirname(os.path.abspath(__file__))
        files_from_sources = os.listdir(current_path + "/source")
        templates = list(filter(lambda x: x != "__init__.py", files_from_sources))
        return templates

    def get_templates(self):
        # convert list of string to list of object with attr name
        return list(map(lambda x: {"name": x}, self.templates))