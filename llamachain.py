import json
import os

from langchain import OpenAI, LLMChain, PromptTemplate
from langchain.memory import ConversationBufferMemory

open_ai_key = os.environ.get("OPENAI_API_KEY")


class OpenAIChat:
    def __init__(self):
        template = """You are a generator of commands without extra details only commands for the Evolver CLI.(only output commands)
        {rules_of_creation_commands}
        Human: {human_input}
        Chatbot:"""

        prompt = PromptTemplate(
            input_variables=["rules_of_creation_commands", "human_input"], template=template
        )
        memory = ConversationBufferMemory(memory_key="rules_of_creation_commands")
        file = open("./templates/source/learn.json", "r")
        messages = json.load(file)

        memory.chat_memory.add_user_message(
            messages["history"]
        )

        memory.chat_memory.add_ai_message("Perfecto")
        self.llm_chain = LLMChain(
            llm=OpenAI(openai_api_key=open_ai_key, model_name="gpt-3.5-turbo",
                       max_tokens=3700, temperature=0.5, top_p=1.0, frequency_penalty=0.0, presence_penalty=0.0),
            prompt=prompt,
            verbose=True,
            memory=memory,
        )

    def predict(self, human_input):
        return self.llm_chain.predict(human_input=human_input)